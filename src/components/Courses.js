import React from 'react';

//css
import styles from "../scss/Courses.module.css";

//component
import Course from "./Course"

const Courses = () => {


    const Products = [
        {
            id:'1' ,
            title:"دوره حرفه ای متخصص ریکت و ریداکس" ,
            time:"43 ساعت" ,
            link:"CoursesDetailes" ,
            img:"React.svg",
            price:2298000
        },
        {
            id:'2' ,
            title:" Next.js دوره حرفه ای متخصص" ,
            time:"40 ساعت" ,
            link:"CoursesDetailes" ,
            img:"nextjs.svg",
            price:1888000
        },
        {
            id:'3' ,
            title:"دوره جامع و پیشرفته جاوااسکریپت" ,
            time:"30 ساعت" , 
            link:"CoursesDetailes" ,
            img:"javascript.svg",
            price:1539000
        },
        {
            id:'4' ,
            title:" TailwindCSS دوره حرفه ای متخصص" ,
            time:"14 ساعت" , 
            link:"CoursesDetailes" ,
            img:"tailwindCSS.svg",
            price:498000
        },
        {
            id:'5' ,
            title:"دوره طراحی وب ریسپانسیو" ,
            time:"16 ساعت" , 
            link:"CoursesDetailes" ,
            img:"html-css.svg",
            price:398000
        },
        {
            id:'6' ,
            title:"دوره در مسیر فریلنسری" ,
            time:"6 ساعت" , 
            link:"CoursesDetailes" ,
            img:"freelancering.svg",
            price:329000
        }
    ]



    return (
        <>
            <div className={styles.titleContainer}>
                <h1 className={styles.title}>دوره های برنامه نویسی آکادمی فرانت هوکس</h1>
                <span></span>
            </div>
            <div className={styles.container}>

                {
                    Products.map( product => <Course  
                                    key={product.id}
                                    productData={product}
                        />)
                }

            </div>
        </>
    );
};

export default Courses;