import React, { useEffect, useState } from 'react';

import { Validate } from '../errors/Validate';

//component
import Footer from "./Footer"

//css
import styles from "../scss/Login.module.css"

const Login = () =>{

    const [ data, setData ] = useState({
        email:""
    });
    const [ errors, setErrors ] = useState({});

    const changeHandler = event => {
        setData({...data, [event.target.name] : event.target.value})
    };

    useEffect( () => {
        setErrors(Validate(data))
    },[data]);

    const submitHandler = event => {
        event.preventDefault();
        if (Object.keys(errors).length) {
            alert(errors.email)
        }
    }

    return (
        <>
            <div className={styles.container}>
                <form onSubmit={submitHandler}>
                    <p className={styles.title}>آموزش</p>
                    <p className={styles.registerTitle}>ورود/ ثبت نام</p>
                    <p className={styles.emailBoxLable}>ایمیل خود را وارد کنید</p>
                    <input
                        type="text" 
                        name='email'
                        value={data.email}
                        onChange={changeHandler}
                    />
                    <button className={styles.inter} type='submit'>ورود به آموزش</button>
                </form>
            </div>
            <Footer/>
        </>
    );
};

export default Login;
