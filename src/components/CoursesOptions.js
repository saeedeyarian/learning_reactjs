import React from 'react';

//css
import styles from "../scss/CoursesOptions.module.css";

//images
import Practical from "../images/practical.svg";
import time from "../images/time.svg";
import mentor from "../images/mentor.svg";
import quality from "../images/quality.svg";
import refund from "../images/refund.svg";

const CoursesOptions = () => {
    return (
        <div className={styles.container}>

            <div className={styles.options}>
                <img src={Practical} alt="practical"/>
                <p>کاربردی و پروژه محور</p>
                <p> با کار روی پروژه های واقعی <br />
                    بازار رو از نزدیک لمس کنید
                </p>
            </div>

            <div className={styles.options}>
                <img src={time} alt="time"/>
                <p>ویدیو های کوتاه و با کیفیت</p>
                <p> هر مبحث در قالب یک جلسه<br />
                    کوتاه و کاربردی آماده شده است
                </p>
            </div>

            <div className={styles.options}>
                <img src={mentor} alt="mentor"/>
                <p>همراهی مربی</p>
                <p> با مربی های حرفه ای و با حوصله <br />
                    رفع اشکال می کنید
                </p>
            </div>

            <div className={styles.options}>
                <img src={quality} alt="quality"/>
                <p> تضمین کیفیت</p>
                <p> بهترین پشتیبان و به روز ترین<br />
                    سطح آموزش موجود در ایران رو<br />
                    دریافت می کنید
                </p>
            </div>

            <div className={styles.options}>
                <img src={refund} alt="refund"/>
                <p> تضمین بازگشت وجه</p>
                <p> اگر راضی نبودید تا 15 روز<br />
                    فرصت دارید انصراف بدید
                </p>
            </div>

        </div>
    );
};

export default CoursesOptions;