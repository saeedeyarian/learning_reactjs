import React from 'react';
import { Link } from 'react-router-dom';

//css
import styles from "../scss/Courses.module.css";

//component
import Course from "./Course";
import Footer from "./Footer";

const Courses = () => {


    const Products = [
        {
            id:1 ,
            title:"دوره حرفه ای متخصص ریکت و ریداکس" ,
            time:"43 ساعت" ,
            link:"/education/ReactCourses" ,
            img:"React.svg",
            price:"2,298,000 تومان "
        },
        {
            id:2 ,
            title:" Next.js دوره حرفه ای متخصص" ,
            time:"40 ساعت" ,
            link:"/education/ReactCourses" ,
            img:"nextjs.svg",
            price:"تومان 1,888,000"
        },
        {
            id:3 ,
            title:"دوره جامع و پیشرفته جاوااسکریپت" ,
            time:"30 ساعت" , 
            link:"/education/ReactCourses" ,
            img:"javascript.svg",
            price:"1,539,000 تومان "
        },
        {
            id:4 ,
            title:" TailwindCSS دوره حرفه ای متخصص" ,
            time:"14 ساعت" , 
            link:"/education/ReactCourses" ,
            img:"tailwindCSS.svg",
            price:"498,000 تومان "
        },
        {
            id:5 ,
            title:"دوره طراحی وب ریسپانسیو" ,
            time:"16 ساعت" , 
            link:"/education/ReactCourses" ,
            img:"html-css.svg",
            price:" 398,000 تومان "
        },
        {
            id:6 ,
            title:"دوره در مسیر فریلنسری" ,
            time:"6 ساعت" , 
            link:"/education/ReactCourses" ,
            img:"freelancering.svg",
            price:"329,000 تومان "
        }
    ]



    return (
        <>
            <div className={styles.EducationContainer}>
                <ul>
                    <Link to="/Landing"><li>خانه</li></Link>
                    <li> » </li>
                    <Link to="/Landing/Education" style={{fontWeight:"bold"}}><li>دوره های آموزشی</li></Link>
                </ul>
                <div className={styles.container}>

                    {
                        Products.map( product => <Course  
                                        key={product.id}
                                        productData={product}
                            />)
                    }

                </div>
            </div>
            <Footer/>
        </>
    );
};

export default Courses;