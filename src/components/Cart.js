import React, { useContext } from 'react';

//image
import check from "../images/Check.svg";
import trash from "../images/trash.svg";

//context
import { cartContext } from '../context/CartContextProvider';

//css
import styles from "../scss/Cart.module.css";

const Cart = ({data}) => {

    const {dispatch} = useContext(cartContext)
    
    return (
        <div className={styles.container}>
            <section className={styles.mainSection}>
                <div className={styles.productImage}>
                    <img src={require(`../images/${data.img}`)} alt="product"/> 
                </div>
                <section className={styles.cartsTexts}>
                    <p>{data.title}</p>
                    <div>
                        <img src={check} alt="check"/>
                        <span>پشتیبانی مادام العمر</span>
                    </div>
                    <div>
                        <img src={check} alt="check"/>
                        <span>آپدیت رایگان دوره</span>
                    </div>
                    <div>
                        <img src={check} alt="check"/>
                        <span>دوره پروژه محور</span>
                    </div>
                    <div>
                        <img src={check} alt="check"/>
                        <span>مدت زمان دانلود، نامحدود</span>
                    </div>
                    <div className={styles.price}>{data.price} تومان</div>
                    <div className={styles.buttons}>
                        <div>
                            <span>{data.quantity}</span>
                        </div>
                        <button onClick={() => dispatch ({type:"REMOVE-ITEM", payload:data})}>
                            <img src={trash} alt="trash"/>
                        </button>
                    </div>
                </section>
            </section>
        </div>
    );
};

export default Cart;