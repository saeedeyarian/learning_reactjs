export const Validate = (data) => {
    const errors = {};

    if (!/\S+@\S+\.\S+/.test(data.email)) {
        errors.email = ("ایمیل معتبر را وارد کنید.")
    }

    return errors
}