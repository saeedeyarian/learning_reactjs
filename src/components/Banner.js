import React from 'react';

//css
import styles from "../scss/Banner.module.css";

//img
import banner from "../images/banner.webp"
import { Link } from 'react-router-dom';

const Banner = () => {
    return (
        <div className={styles.container}>
            <div className={styles.banner}>
                <img src={banner} alt="banner" className={styles.bannerImage}/>
                <div className={styles.bannerTexts}>
                    <h1>دوره های آموزشی آکادمی فرانت هوکس</h1>
                    <p>برنامه نویسی را سریع، آسان و پروژه محور یادبگیرید</p>
                    <Link to="/Landing/Education">
                        <button>مشاهده دوره ها</button>
                    </Link>
                    <div className={styles.frontPaln}>
                        <p>اگر هنوز برات سواله که چه دوره ای برات مفیده؟</p>
                        <p>حتما مقاله نقشه راه فرات اند رو بخون</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Banner;