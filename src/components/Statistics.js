import React from 'react';

//css
import styles from "../scss/Statistics.module.css";

const Statistics = () => {
    return (
        <div className={styles.container}>

            <div className={styles.texts}>
                <p>آمارها باعث افتخار ما هستند </p>
                <p>آخرین به روز رسانی:</p>
            </div>

            <div className={styles.statistics}>
                <div>
                    <p>5+</p>
                    <p>سال سابفه فعالیت حرفه ای</p>
                </div>
                <div>
                    <p>2,433+</p>
                    <p> دانشجوی خصوصی و آنلاین</p>
                </div>
                <div>
                    <p> 97 % +</p>
                    <p>رضایت از آموزش</p>
                </div>
            </div>
        </div>
    );
};

export default Statistics;