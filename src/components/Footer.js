import React from 'react';
import { Link } from 'react-router-dom';

//css
import styles from "../scss/Footer.module.css";

//image
import zarinPal from "../images/zarin.svg";


const Footer = () => {
    return (
        <footer>
            <div className={styles.footerContainer}>
                    <div className={styles.footerTexts}>
                        <p>
                            <span>آکادمی آنلاین فرانت هوکس،دوره های کوچ محور برنامه نویسی وب</span><br />
                            <span style={{color:"#464e57"}}>صاحب محمدی هستم، برنامه نویس وب.</span> از سال 93 به برنامه نویسی مشغول<br/>
                                هستم و بیش از سه سال هست که به صورت تخصصی به عنوان توسعه دهنده<br />
                                فرانت، در استارتاپ هایی مثل دکوژ، سهم خلاق و صرافی های آنلاین مشفول به <br />
                                فعالیت بودم. <br />
                                دراین چندسال چالش های زیادی رادرزمینه برنامه نویسی چه به صورت کار<br />
                                فریلنسری درسایتهای خارجی همچنین درکارتیمی تجربه کرده ام. هزینه مالی و<br />
                                زمانی بسیارزیادی رابابت کسب آموزش درمعتبرترین دوره های سایتهای خارجی <br />
                                صرف کردم. همه ی آموزش های فرانکت هوکس برپایه سه اصل ،دوره های باکیفیت<br />
                                وپروژه محور و به روز، پشتیبانی مادام العمر به همراه آپدیت های رایگان <br />
                                وعودت هزینه درصورت هرگونه نارضایتی استوار است .امیدوارم فرانت هوکس مراه خوبی <br/>
                                برای ورود شما به بازارکار ودنیای برنامه نویسی باشد. شمامی توانید ازطریق<br />
                                <span style={{fontWeight:"normal"}}>اینستاگرام تلگرام یا لینکدین</span> با من درارتباط باشید.
                        </p>
                    </div>
                    <div  className={styles.footerLinks}>
                        <p>بخش های سایت</p>
                        <ul className={styles.mainLinks}>
                            <Link to="/Landing/Education"><li> دوره های آموزشی</li></Link>
                            <Link><li> بلاگ های آموزشس</li></Link>
                            <Link><li> درباره ما </li></Link>
                            <Link><li> شروع یادگیری </li></Link>
                        </ul>
                    </div>
                    <div  className={styles.footerLinks}>
                        <p>دوره های آمورشی</p>
                        <ul className={styles.coursesLinks}>
                            <Link><li>دوره متخصص ریکت و ریداکس</li></Link>
                            <Link><li>دوره متخصص Next.js</li></Link>
                            <Link><li>دوره پیشرفته جاوااسکریپت</li></Link>
                            <Link><li>دوره پروژه نحور تیلویند</li></Link>
                            <Link><li>دوره طراحی وب ریسپانسیو </li></Link>
                        </ul>
                    </div>
                    <div className={styles.zarinPal}>
                        <img src={zarinPal} alt="zarinPal" />
                    </div>
            </div>
        </footer>
    );
};

export default Footer;