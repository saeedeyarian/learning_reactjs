import React from 'react';


//components
import Banner from "./Banner";
import Courses from './Courses';
import CoursesOptions from './CoursesOptions';
import FrontendIntro from './FrontendIntro';
import Statistics from './Statistics';
import Footer from "./Footer"

const LandingPage = () => {
    return (
        <div>
            <Banner/>
            <CoursesOptions/>
            <Courses/>
            <FrontendIntro/>
            <Statistics/>
            <Footer/>
        </div>
    );
};

export default LandingPage;