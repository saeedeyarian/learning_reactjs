import React, { useContext } from 'react';
import { Link } from "react-router-dom"

//component
import Cart from './Cart';
import Footer from "./Footer"

//context
import { cartContext } from '../context/CartContextProvider';

//css
import styles from "../scss/ShoppingCart.module.css"


const ShoppingCart = () => {

    const {state} = useContext(cartContext);

    const discount = state.total - 100000

    return (
        <>
            <div className={styles.container}>
                {state.itemCounter > 0 ?
                    <>
                    <section className={styles.cartsSection}>
                        {
                            state.selectedItems.map ( item => <Cart key={item.id} data={item}/>)
                        }
                    </section> 
                    <section className={styles.paymentSection}>
                        <div className={styles.payment}>
                            <section>
                                <div>
                                    <span>قیمت کالاها</span>
                                    <span>{state.total}</span>
                                </div>
                                <div>
                                    <span>تخفیف کالاها</span>
                                    <span>100,000 </span>
                                </div>
                            </section>
                            <div className={styles.total}>
                                <span>جمع سبد خرید</span>
                                <span>{discount}</span>
                            </div>                        
                            <Link>
                                <button className={styles.continue}>ادامه سفارش</button>
                            </Link>
                        </div>
                </section>
                    </> :
                <div>سبد خرید شما خالی است</div>
                    }           
            </div>
        <Footer/>
    </>
    );
};

export default ShoppingCart;