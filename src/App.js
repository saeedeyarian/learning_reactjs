import { Route, Routes, Navigate } from 'react-router-dom';
import './App.css';

//components
import Header from './components/Header';
import Landing from "./components/LandingPage"
import Login from "./components/Login";
import ShoppingCart from "./components/ShoppingCart";
import Education from "./components/EducationCourses";
import CartContextProvider from './context/CartContextProvider';
import Blogs from './components/BlogsPage';
import Exam from './components/Exam';
import CoursesDetailes from './components/CoursesDetailes';

function App() {
  return (
    <CartContextProvider>
      <Header/>
      <Routes>
        <Route path='/Landing' element={<Landing/>}/>
        <Route path="/Landing/Login" element={<Login/>}/>
        <Route path="/Landing/ShoppingCart" element={<ShoppingCart/>}/>
        <Route path="/Landing/Education" element={<Education/>}/>
        <Route path='/Landing/Blogs' element={<Blogs/>}/>
        <Route path='/Landing/CoursesDetailes' element={<CoursesDetailes/>}/>
        <Route path='/Landing/Exam' element={<Exam/>}/>
        <Route path="/*" element={<Navigate to="/Landing"/>}/>
      </Routes>
    </CartContextProvider>
  );
}

export default App;
