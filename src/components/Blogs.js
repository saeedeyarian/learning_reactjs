import React from 'react';
import { Link } from 'react-router-dom';


//css
import styles from '../scss/Blogs.module.css';
//image
import vue from '../images/logos_vue.png'


const Course = ({blogData}) => {

    return (
        <div className={styles.container}>
            <div className={styles.Image}>
                <img alt='course' src={vue}/>
            </div>
            <div  className={styles.titleConainer}>
                <Link to={`/landing/${blogData.link}`}>
                    <p>{blogData.title}</p>
                    <p className={styles.category}>{blogData.category}</p>
                </Link>
            </div>
        </div>
    );
};

export default Course;