import React from 'react';
import { Link } from "react-router-dom";
import { useContext } from 'react';

//css
import styles from '../scss/Course.module.css';

//image
import clock from "../images/Clock.svg";
import arrow from "../images/Arrow - Left.svg";

import { isInCart } from '../functions/functions';
import {cartContext} from "../context/CartContextProvider";


const Course = ({productData}) => {

    const { state,dispatch } = useContext(cartContext)

    return (
        <div className={styles.coursesItems}>
            <div className={styles.coursesImage}>
                <img alt='course' src={require(`../images/${productData.img}`)}/>
            </div>
            <p className={styles.coursesName}>{productData.title}</p>
            <div className={styles.coursesTime}>
                <img src={clock} alt="clock"/>
                <p>{productData.time}</p>
            </div>
            <Link to={`/Landing/${productData.link}`} className={styles.coursesDetails}>
                <p>مشاهده دوره</p>
                <img src={arrow} alt="arrow"/>
            </Link>
            <div className={styles.register}>
                {
                    isInCart(state,productData.id) ?
                    <button>ادامه سفارش</button> :
                    <button onClick={() => dispatch({type:"ADD_ITEM", payload: productData})}>ثبت نام دوره</button> 
                }
                <p>{productData.price} تومان </p>
            </div>
        </div>
    );
};

export default Course;