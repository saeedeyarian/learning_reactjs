import React, { useContext, useState } from 'react';
import { Link } from "react-router-dom";
import { cartContext } from '../context/CartContextProvider';

//image
import logo from "../images/fh-logo.svg";
import shoppingCart from "../images/shoppingcart.svg";
import login from "../images/User.png";
import arrow from "../images/Arrow - Bottom.svg"

//css
import styles from "../scss/Header.module.css"

const Header = () => {

    const {state} = useContext(cartContext)

    const [show,setShow] = useState(false)

    const showHandler = event => {
        event.preventDefault();
        setShow(!show)
    }

    return (
        <header className={styles.container}>
            <div className={styles.headerContainer}>

                <div className={styles.navbarMenue}>      
                        <div className={styles.logo}>
                            <img src={logo} alt="logo"/>
                        </div>

                    <ul className={styles.navbarItems}>
                        <Link to="/Landing"><li>صفحه اصلی</li></Link>
                        <Link to="/Landing/Education"><li>دوره های آموزشی</li></Link>
                        <Link to="/Landing/Blogs"><li>بلاگ ها</li></Link>
                        <Link className={styles.connectionLinks} onClick={showHandler} >
                            <img src={arrow} alt={arrow}/>
                            <li>ارتباط با ما
                                <div className={show ? styles.connectionSubmenue : styles.hide}>
                                    <Link><p>درباره ما</p></Link>
                                    <Link><p>کانال تلگرام</p></Link>
                                    <Link><p>صفحه اینستاگرام</p></Link>
                                </div>
                            </li>
                        </Link>
                    </ul>
                </div>

                <div className={styles.loginShopcartContainer}>
                    <div className={styles.shoppingCart}>
                        <Link to="/Landing/ShoppingCart">
                            <img src={shoppingCart} alt="shoppingcart"/>
                            <p className={styles.quantity}>{state.itemCounter}</p>
                        </Link>
                    </div>

                    <div className={styles.loginContainer}>
                        <Link to="/Landing/Login">
                            <img src={login} alt="loginIcon" className={styles.loginIcon}/>
                        </Link>
                    </div>
                </div>

            </div>
        </header>
    );
};

export default Header;