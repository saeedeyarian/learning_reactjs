import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';


//css
import styles from '../scss/BlogsPage.module.css';

// //component
import Blogs from './Blogs';
import Footer from './Footer'

//image
import arrow from "../images/Arrow - Bottom.svg";


const BlogsPage = () => {


    const Blog = [
        {
            id:'1' ,
            title:" ریداکس چیست؟کاربردی ریداکس تانک چیست؟" ,
            link:"Exam" ,
            category:"ریکت"
        },
        {
            id:'2' ,
            title:"ریکت هوک چیه؟" ,
            link:"Exam" ,
            category:"ریکت"
        },
        {
            id:'3' ,
            title:"بک اند چیه" ,
            link:"Exam" ,
            category:"بک اند"
        },
        {
            id:'4' ,
            title:"نود جی اس چیه" ,
            link:"Exam" ,
            category:"بک اند"
        },
        {
            id:'5' ,
            title:"ویو جی اس چیه؟" ,
            link:"/education/Exam",
            category:"vue.js"
        },
        {
            id:'6' ,
            title:"ریکت چیست؟" ,
            link:"Exam" ,
            category:"ریکت"

        }
    ]

const [data, setData] = useState([])
const [style, setStyle] = useState(false)
const [rotate , setRotate] = useState(false)

//rotateArrow in category
const arrowRotate = () =>{
    setRotate(!rotate)
}


//paginate
const spliceSecondDataHandler = () => {
        const nextDatat= Blog.slice(3,6)
        setData(nextDatat)
        setStyle(!style)
    }

const sliceFirstDataHandler = () => {
        const Data = Blog.splice(0,3)
        setData(Data)
        setStyle(!style)
    }


useEffect( () => {
        const Data = Blog.splice(0,3)
        setData(Data)
    },[])


//filter

const reactFilterHandler = () => {
    const newData = Blog.filter(item => item.category === 'ریکت')
    setData(newData)
}
const backendFilterHandler = () => {
    const newData = Blog.filter(item => item.category === 'بک اند')
    setData(newData)
}

const vueFilterHandler = () => {
    const newData = Blog.filter(item => item.category === 'vue.js')
    setData(newData)
}

const notBlog = (item) =>  {
    const newData = Blog.filter(i => i.id === item.id)
    setData(newData)
}


    return (
        <>
        <div className={styles.container}>
            <ul style={{marginBottom:"50px"}}>
                <Link to="/Landing"><li>خانه</li></Link>
                <li> » </li>
                <Link to="/Landing/Education" style={{fontWeight:"bold"}}><li> بلاگ ها </li></Link>
            </ul>
            <div className={styles.sidesContainer}>
                <aside className={styles.leftSide}>
                    <div className={styles.blogsContainer}>
                        { 
                            data &&
                                data.map( blog =>
                                    <Blogs key={blog.id}
                                        blogData={blog}/>
                                )
                        }
                    </div>
                    <nav className={styles.paginateNumbers}>
                         <ul>
                            <li onClick={sliceFirstDataHandler} className={!style ? styles.arrowSelection : styles.arrowNotSelection}>
                                <img src={arrow} alt="arrowIcon"/>
                            </li>
                            <li onClick={sliceFirstDataHandler} className={!style ? styles.selection : styles.notSelection}>
                                1
                            </li>
                            <li onClick={spliceSecondDataHandler} className={style ? styles.selection : styles.notSelection}>
                                2
                            </li>
                            <li onClick={spliceSecondDataHandler} className={style ? styles.arrowSelection : styles.arrowNotSelection}>
                                <img src={arrow} alt="arrowIcon" className={styles.arrow}/>
                            </li>
                         </ul>
                     </nav>
                </aside>
                <aside className={styles.rightSide}>
                    <div className={styles.categories}>
                         <div onClick={arrowRotate}>
                             دسته بندی مقالات
                             <img src={arrow} alt='arrow' className={rotate ? styles.changeRotate : styles.rotateTransition}/>
                         </div>
                         <nav className={rotate && styles.hideMenue }>
                            <ul>
                                <li onClick={sliceFirstDataHandler}>همه پست ها</li>
                                <li onClick={notBlog}>جاوااسکریپت</li>
                                <li onClick={reactFilterHandler}>ریکت</li>
                                <li onClick={vueFilterHandler}>Vue.js</li>
                                <li onClick={notBlog}>عمومی</li>
                                <li onClick={notBlog}>Next.js</li>
                                <li onClick={backendFilterHandler}>بک اند</li>
                            </ul>
                         </nav>
                     </div>
                </aside>
            </div>
        </div>
        <Footer/>
        </>
    );
};

export default BlogsPage;