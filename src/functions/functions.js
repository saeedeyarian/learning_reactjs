const isInCart = (state, id) => {
    const result = !!state.selectedItems.find(item => item.id === id)
    return result;
}

const quantityCount = (state , id) => {
    const index = state.selctedItems.findIndex ( item => item.id === id)
    if (index === -1) {
        return false
    } else {
        return state.selctedItems[index].quantity
    }
}

export { isInCart , quantityCount}