import React from 'react';
import  { Link } from "react-router-dom";

//image
import frontIntro from "../images/frontend-intro.svg";

//css
import styles from "../scss/FrontendIntros.module.css";

const FrontendIntro = () => {
    return (
        <div className={styles.container}>
            <div className={styles.introImage}>
                <img src={frontIntro} alt="frontend-intro"/>
            </div>
            <div className={styles.introText}>
                <p>فرانت اند (Front-end) چیه؟</p>
                <p>بزار خیلی خودمونی و به دور از تعاریف کلیشه ای بهت بگم فرانت اند(Front-end) چیه؟کیه و چیکار میکنه؟ هر وبسایت و یا <br />
                    اپلیکیشنی دو بخش اساسی داره. یکی سمت سرور که کاربر نمیبینه و یکی سمت کاربرکه میشه همون ظاهر سایت. دقیقا درست حدس  <br />
                    زدی به چیزایی که ما مستقیما باهاش کار میکنیم و میبینیم میگن فرانت سایت. مثل هدر،فوتر،بخش اصلی محتوای سایت،فرم های <br />
                    ثبت نام و...که پیاده کردن همه این ها به عهده توسعه دهنده فرانت(front-end developer) هست . با اومدن فریمورک های جذاب <br/>
                    جاوااسکریپت مثل vue.js و کتابخونه ریکت (react.js) وظیفه فرانت خیلی سنگین تر شده و خیلی از کارا که توسط توسعه دهنده بک <br/>
                     اند (back-end developer) انجام میشد، الان دیگه فرانت اند کارا انجام میدن و همین دلیل باعث شده بازار استخدامی فرانت اند خیلی <br />
                    داغ بشه و حقوق فرانت اند هم خیلی بیشتر شده و  همه دنبال افراد متخصص توی این حوزه هستند. بهت تبریک میگم به دنیای پر رمزو و <br />
                    راز و جذاب برنامه نویسی وب و مخصوصا فرانت، خوش اومدی.از تجربه ما استفاده کن و تا لحظه ورود به بازار کار با نهایت افتخار <br />
                    کنارتیم.<br />
                </p>
                <p>میخوای بدونی چه دوره ای برای شما مفید هست ؟ <Link>حتما مقاله نقشه راه فرانت اند رو مطالعه کن.</Link></p>
            </div>
        </div>
    );
};

export default FrontendIntro;